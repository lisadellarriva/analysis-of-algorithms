//-------------------------------------------//
// MC458 - Análise de Algoritmo I - 2s2016
// Laboratório 5
// Aluna: Elisa Dell'Arriva - RA: 135551
//-------------------------------------------//

#include <vector>
#include <iostream>
#include <utility>
#include <stdlib.h>

using namespace std;

//tempo dos para natação, ciclismo e corrida, respectivamente.
struct Time {
  int s,p,c;
};

//indice e valor de (corrida+ciclismo) de cada participante
struct Participante {
  int indice, criteria;
};

// @function      myIntercala
// @description   intercala dois vetores para deixá-los ordenados; auxiliar à função myMergeSort
// @params        input: vetor em que as inversões são contadas
//                i: índice do início de input
//                m: índice do meio de input
//                f: índice do final de input
void myIntercala(vector<Participante> &input, int i, int m, int f) {
    //Variáveis
    vector<Participante> aux(input.size());

    for (int a=i; a<=m; a++) {
      aux[a].indice = input[a].indice;
      aux[a].criteria = input[a].criteria;
    }
    for (int b=m+1; b<=f; b++) {
      aux[f+m+1-b].indice = input[b].indice;
      aux[f+m+1-b].criteria = input[b].criteria;
    }

    int a = i;
    int b = f;
    for (int k=i; k<=f; k++) {
        if (aux[a].criteria >= aux[b].criteria) { //aux[a] >= aux[b]
          input[k].indice = aux[a].indice;
          input[k].criteria = aux[a].criteria;
          a = a + 1;
        }
        else { //aux[a] < aux[b]
          input[k].indice = aux[b].indice;
          input[k].criteria = aux[b].criteria;
          b = b - 1;
        }
    }
}

// @function      myMergesort
// @description   ordena participantes pelo valor de (corrida + ciclismo)
// @params        equipe: vetor com participantes e seus valores de tempo para corrida+ciclismo
//                i: índice do início de equipe
//                m: índice do meio de equipe
//                f: índice do final de equipe
void myMergesort(vector<Participante> &equipe, int tamanho) {
  int passo;
  int inicioE, inicioD, meio;

  for (passo=1; passo <= tamanho-1; passo = 2*passo) {
    for (inicioE=0; inicioE < tamanho-1; inicioE = inicioE + 2*passo) {
      meio = inicioE + passo - 1;
      if (meio >= tamanho) {
        meio = (inicioE + inicioD)/2;
      }

      if ((inicioE + 2*passo - 1) < (tamanho - 1)) {
        inicioD = inicioE + 2*passo - 1;
      }
      else {
        inicioD = tamanho - 1;
      }
      myIntercala(equipe, inicioE, meio, inicioD);
    }
  }
}

//a funcao deve retornar os indices dos atletas na ordem de participacao.
vector<int> solution(const vector<Time> & input) {
  vector<int> order(input.size());
  vector<Participante> equipe(input.size());

  for (int i=0; i<input.size(); i++) {
    equipe[i].indice = i;
    equipe[i].criteria = input[i].p + input[i].c;
  }

  myMergesort(equipe, input.size());

  for (int i=0; i<input.size(); i++) {
    order[i] = equipe[i].indice;
  }

  return order;
}
