#include <vector>
#include <iostream>
#include <utility>
#include <stdlib.h>

using namespace std;
//retorna o custo total como descrito no enunciado
int solution(const vector<int> & input, int L) {
  int                  n = input.size(),
                       numeroBemGrande = 1000000000;
  vector<vector<int> > folgas(n+1, vector<int> (n+1)),
                       custoFileiras(n+1, vector<int> (n+1));
  vector<int>          custoTotal(n+1);


  // calcula número de folgas das bancadas i a j ficarem na mesma fileira,
  // com 1 <= i < j <= n
  for (int i=1; i <= n; i++) {
    folgas[i][i] = L - (input[i-1]);
    for (int j=i+1; j <= n; j++) {
      folgas[i][j] = folgas[i][j-1] - input[j-1] - 1;
    }
  }

  // calcula o custo de colocar bancadas i a j na mesma fileira
  for (int i=1; i <= n; i++) {
    for (int j=i; j <= n; j++) {
      if (folgas[i][j] < 0) {
        custoFileiras[i][j] = numeroBemGrande;
      }
      else if (j == n && (folgas[i][j] == 0)) {
        custoFileiras[i][j] = 0;
      }
      else {
        custoFileiras[i][j] = folgas[i][j]*folgas[i][j];
      }
    }
  }

  // calcula o custo do arranjo de fileiras que gera custo mínimo
  custoTotal[0] = 0;
  for (int j=1; j <= n; j++) {
    custoTotal[j] = numeroBemGrande;
    for (int i=1; i <= j; i++) {
      if (custoTotal[i-1] != numeroBemGrande && custoFileiras[i][j] != numeroBemGrande && (custoTotal[i-1] + custoFileiras[i][j] < custoTotal[j])) {
        custoTotal[j] = custoTotal[i-1] + custoFileiras[i][j];
      }
    }
  }

  return custoTotal[n];
}
