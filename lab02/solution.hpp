//-------------------------------------------//
// MC458 - Análise de Algoritmo I - 2s2016
// Laboratório 2
// Aluna: Elisa Dell'Arriva - RA: 135551
//-------------------------------------------//

#include "pint.hpp"
#include <vector>
#include <iostream>
#include <utility>
#include <stdlib.h>

using namespace std;

// @function      myIntercala
// @description   intercala dois vetores para deixá-los ordenados; auxiliar à função myMergeSort
// @params        input: vetor em que as inversões são contadas
//                i: índice do início de input
//                m: índice do meio de input
//                f: índice do final de input
//                indexes: vetor com os índices inicias do vetor input
int myIntercala(vector<Pint> &input, int i, int m, int f, vector<int> &indexes){
    //Variáveis
    vector<Pint> aux(input.size(), 0);
    vector<int> auxIndexes(input.size(), 0);
    int trocas;

    for (int a=i; a<=m; a++) {
      aux[a] = input[a];
      auxIndexes[a] = indexes[a];
    }
    for (int b=m+1; b<=f; b++) {
      aux[f+m+1-b] = input[b];
      auxIndexes[f+m+1-b] = indexes[b];
    }

    int a = i;
    int b = f;
    trocas = 0;
    for (int k=i; k<=f; k++) {
        if (aux[a].compare(aux[b]) <= 0){ //aux[a] <= aux[b]
          input[k] = aux[a];
          indexes[k] = auxIndexes[a];
          a = a + 1;
        }
        else { //aux[a] > aux[b]
          input[k] = aux[b];
          indexes[k] = auxIndexes[b];
          b = b - 1;
          trocas = trocas + (m - a + 1);
        }
    }
    return trocas;
}

// @function      myMergesort
// @description   conta o número de inversões em input, ordena input e o vetor de indexes de input
// @params        input: vetor em que as inversões são contadas
//                i: índice do início de input
//                m: índice do meio de input
//                f: índice do final de input
//                indexes: vetor com os índices inicias do vetor input
int myMergesort(vector<Pint> &input, int i, int f, vector<int> &indexes) {

    if (i < f) {
        int m = (f+i)/2;
        return myMergesort(input, i, m, indexes) + myMergesort(input, m+1, f, indexes) + myIntercala(input, i, m, f, indexes);
    }
    return 0;
}

// @function      getMaxDistancia
// @description   retorna a maior distância |P(I) - I|
// @params        indexes: vetor com os índices ordenados do vetor input
int getMaxDistancia(vector<int> &indexes) {
    int maior = 0,
        diff;

    for (int i=0; i<indexes.size(); i++) {
        diff = indexes[i] - i;
        if (diff < 0) {
          diff = diff*-1;
        }
        if (diff > maior) {
            maior = diff;
        }
    }
    return maior;
}

//retorna o custo total
int solution(vector<Pint> & input) {
  //Variáveis
  vector<int> indexes(input.size());    //vetor com índices do vetor input
  int OT = 0,                           //ordem de troca do vetor input
      M = 0;                            //maior distância |P(I) - I|

  //Inicialização do vetor indexes
  for (int i=0; i<indexes.size();i++) {
    indexes[i] = i;
  }

  OT = myMergesort(input, 0, input.size()-1, indexes);  //Conta inversões OT e ordena vetor de índices
  M = getMaxDistancia(indexes);                         //Determina a distância máxima |P(I) - I|

  return (OT * M);
}
