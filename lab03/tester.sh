#!/bin/bash

for i in {1..9}
do
    ./lab < testes/arq$i.in > testes/myresp$i
    echo "TEST 0$i: "
    diff testes/arq$i.res testes/myresp$i
done

for i in {10..20}
do
    ./lab < testes/arq$i.in > testes/myresp$i
    echo "TEST $i: "
    diff testes/arq$i.res testes/myresp$i
done
