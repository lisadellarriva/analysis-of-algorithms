//-------------------------------------------//
// MC458 - Análise de Algoritmo I - 2s2016
// Laboratório 3
// Aluna: Elisa Dell'Arriva - RA: 135551
//-------------------------------------------//

#include "pint.hpp"
#include <vector>
#include <iostream>
#include <utility>
#include <stdlib.h>

using namespace std;

//pares utilizados para reduzir o código
typedef pair<int,int> pii;
typedef pair<Pint,int> ppi;

// @function      myIntercala
// @description   intercala dois vetores para deixá-los ordenados; auxiliar à função myMergeSort
// @params        input: vetor em que as inversões são contadas
//                i: índice do início de input
//                m: índice do meio de input
//                f: índice do final de input
void myIntercala(vector<ppi> &input, int i, int m, int f){
    //Variáveis
    vector<ppi> aux(input.size());

    for (int a=i; a<=m; a++) {
      aux[a].first = input[a].first;
      aux[a].second = input[a].second;
    }
    for (int b=m+1; b<=f; b++) {
      aux[f+m+1-b].first = input[b].first;
      aux[f+m+1-b].second = input[b].second;
    }

    int a = i;
    int b = f;
    for (int k=i; k<=f; k++) {
        if (aux[a].first.compare(aux[b].first) != -1){ //aux[a] >= aux[b]
          input[k].first = aux[a].first;
          input[k].second = aux[a].second;
          a = a + 1;
        }
        else { //aux[a] < aux[b]
          input[k].first = aux[b].first;
          input[k].second = aux[b].second;
          b = b - 1;
        }
    }
}

// @function      myMergesort
// @description   conta o número de inversões em input, ordena input e o vetor de indexes de input
// @params        input: vetor em que as inversões são contadas
//                i: índice do início de input
//                m: índice do meio de input
//                f: índice do final de input
void myMergesort(vector<ppi> &input, int i, int f) {
    if (i < f) {
        int m = (f+i)/2;
        myMergesort(input, i, m);
        myMergesort(input, m+1, f);
        myIntercala(input, i, m, f);
    }
}

//Troca dois elmentos de um vetor do tipo ppi
void troca(vector<ppi> &v, int a, int b) {
  ppi aux;
  aux.first = v[a].first;
  aux.second = v[a].second;
  v[a].first = v[b].first;
  v[a].second = v[b].second;
  v[b].first = aux.first;
  v[b].second = aux.second;
}

//particiona um vetor
int myParticiona(vector<ppi> &input, int i, int f) {
  Pint pivo = input[f].first;
  int a = i - 1;

  for (int b = i; b<f; b++) {
    if(input[b].first.compare(pivo) == 1) {
      a = a + 1;
      troca(input, a, b);
    }
  }
  troca(input, a+1, f);
  return a + 1;
}
int myParticionaAleatorio (vector<ppi> &input, int i, int f) {
  int j = i + rand() % (f-i+1);
  troca(input, j, f);
  return myParticiona(input, i, f);
}

//Devolve o índice da mkesima maior nota de uma escola
int mySelectNL(vector<ppi> &input, int i, int f, int mkesima) {
  int q, k;

  if (i == f) {
    return i;
  }
  q = myParticionaAleatorio(input, i, f);
  k = q - i + 1;
  if (mkesima == k) {
    return q;
  }
  else if (mkesima < k) {
    return mySelectNL(input, i, q-1, mkesima);
  }
  return mySelectNL(input, q+1, f, mkesima - k);
}

//Ordena vetor por quicksort com particiona aleatório
void myQuicksort(vector<ppi> &input, int i, int f) {
  if (i < f) {
    int q = myParticionaAleatorio(input, i, f);
    myQuicksort(input, i, q-1);
    myQuicksort(input, q+1, f);
  }
}

//retorna o custo total
vector<ppi> solution(vector< vector<ppi> > & input,int M) {
  vector<ppi> out(0);
  int K = input.size();                 //número de escolas participantes
  vector<ppi> mediaPorEscola(K);        //escolas ordenadas pela média de notas de seus alunos
  int mkesimaMaiorNota =  0;            //índice da m/k-ésima maior nota de uma escola

  //Calcula média das notas dos alunos para cada escola
  Pint soma;
  for (int i=0; i<K; i++) {
    soma = 0;
    for (int j=0; j<input[i].size(); j++) {
      soma = soma + input[i][j].first;
    }
    mediaPorEscola[i].first = soma/input[i].size();
    mediaPorEscola[i].second = i;
  }

  //Ordena as escolas de acordo com a média de notas dos alunos
  myMergesort(mediaPorEscola, 0, mediaPorEscola.size()-1);

  //Para cada escola:
  for (int k=0; k<K; k++) {
    if (K < 4) {  //para atender aos primeiros casos, em que não atendem a condicao K*N > 10M
      myQuicksort(input[k], 0, input[k].size());                                //ordena notas dos alunos
    }
    else {
      mkesimaMaiorNota = mySelectNL(input[k], 0, input[k].size(), (M/K+1));     //separa as m/k maiores notas
      if (mkesimaMaiorNota <= 50) {
        myQuicksort(input[k], 0, mkesimaMaiorNota-1);                           //ordena as m/k maiores notas
      }
      else {
        myMergesort(input[k], 0, mkesimaMaiorNota-1);                           //ordena as m/k maiores notas
      }
    }
  }

  //Insere alunos na lista final do processo seletivo da universidade
  out.resize(M);
  int escola;
  int c=0;  //posição do aluno no processo seletivo da universidade
  int i=0;  //classificação do aluno nas escolas
  int j=0;  //índice da escola
  while (c < M) {
    escola = mediaPorEscola[j].second;
    out[c].first = input[escola][i].first;
    out[c].second = input[escola][i].second;
    j = j + 1;
    c = c + 1;
    if (j == K) {
      i = i + 1;
      j = 0;
    }
  }
  return out;
}
