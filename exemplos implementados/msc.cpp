// MC458 - Análise e projeto de algoritmos I
// 2s2016 - Professor Cid
// Aluna: Elisa Dell'Arriva - RA: 135551

// Exemplo 5: Máxima Sequência Consecutiva
// Implementação do algoritmo dado em sala em pseudocódigo

using namespace std;

#include <iostream>
#include <cstdlib>
#include <vector>

const int N = 10;
// @function    computeMSQ
// @description dada uma sequência de números reais, determina a máxima
//              sequência Consecutiva
// @params      i: índice do ínicio da msc
//              j: índice do fim da msc
//              k: índice do ínicio do sufixo máximo
//              maxSeq: valor da soma da msc
//              maxSuf: valor da soma do sufixo máximo
//              seq: sequência de n valores
//              n: tamanho de seq
void computeMSQ(int &i, int &j, int &k, double &maxSeq, double &maxSuf, vector<double> seq, int n) {
  if (n == 1) { // sequencia tem apenas um elemento
    i = 0; j = 0; k = 0;
    if (seq[0] < 0) { // se ele é negatico, a msc é vazia, i.e., 0
      maxSeq = 0; maxSuf = 0;
    }
    else {  // senão, a msc é p próprio elemento único
      maxSeq = 1; maxSuf = 1;
    }
  }
  else {  // sequencia tem mais de um elemento
    computeMSQ(i, j, k, maxSeq, maxSuf, seq, n-1);

    if (k == 0)  // sufixo máximo tem soma negativa
      k = n;     // então o máximo sufixo é o último elemento dessa subsequencia
    maxSuf = maxSuf + seq[n];

    if (maxSuf > maxSeq) { // então o máximo sufixo é a msc
      i = k; j = n;
      maxSeq = maxSuf;
    }
    else if (maxSuf < 0) {  // nesse caso, não há sufixo máximo
      maxSuf = 0;           // por convenção, assume valor zero
      k = 0;
    }
  }
}

// Funcão principal
// Lê entrada e imprime saída
int main() {

  int i, j, k, n;
  double maxSeq, maxSuf;

  // Leitura dos dados de entrada
  // cout << "Digite o valor de n: ";
  cin >> n;

  vector<double> seq(n);

  // cout << "Digite n valores da sequencia: ";
  for (int i=0; i<n; i++) {
    cin >> seq[i];
  }

  // Impressão da sequência
  cout << endl << "Sequência inicial: [ ";
  for (int i=0; i<n; i++) {
    cin >> seq[i];
    cout << seq[i] << " ";
  }
  cout << "]" << endl;

  // Cálculo da máxima sequência consecutiva
  computeMSQ(i, j, k, maxSeq, maxSuf, seq, N);

  // Impressão da saída
  // i -> índice do ínicio da msc e seq[i] o valor do elemento
  // j -> índice do fim da msc e seq[j] o valor do elemento
  // k -> índice do ínicio do máximo sufixo e seq[k] o valor do elemento
  // maxSeq -> o valor da soma dos elementos que compoem a msc
  // maxSeq -> o valor da soma dos elementos que compoem o máximo sufixo
  cout << "---------------------------" << endl;
  cout << "i: " << i << " - seq[i]: " << seq[i] << endl;
  cout << "j: " << j << " - seq[j]: " << seq[j] << endl;
  cout << "k: " << k << " - seq[k]: " << seq[k] << endl;
  cout << "maxSeq: " << maxSeq << endl;
  cout << "maxSuf: " << maxSuf << endl;
  cout << "---------------------------" << endl;

  // Impressão da msc
  cout << "Máxima sequência consecutiva: [ ";
  for (int x=i; x<=j; x++) {
    cin >> seq[x];
    cout << seq[x] << " ";
  }
  cout << "]" << endl;

  return 0;
}
