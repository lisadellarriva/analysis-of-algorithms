#!/bin/bash

echo "Rodando todos testes"

for i in {1..20}
do
  ./lab < testes/arq$i.in > testes/myres$i
  echo "TEST 0$i: "
  diff testes/myres$i testes/arq$i.res
done
