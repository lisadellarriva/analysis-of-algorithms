#include "pint.hpp"
#include <vector>
#include <iostream>
using namespace std;

//retornar o indice (começando em 0) da embalagem mais leve
int solution(const vector<Pint> & input) {
  // Dicas:
  //soma:
  Pint soma = input[0] + input[1];
  input[0].print();
  cout << " + ";
  input[1].print();
  cout << " = ";
  soma.print();
  cout << endl;

  //tamanho da entrada:
  cout << input.size() << endl;
  // comparando: input[99].compare(input[2]) == 0 // iguais
  //             input[99].compare(input[2]) == -1 // input[99] < input[2]
  //             input[99].compare(input[2]) == 1 // input[99] > input[2]

  if (input.size() == 2) {
    if (input[0].compare(input[1]) == 1) {
      return 1;
    }
    else {
      return 0;
    }
  }
  else {

    if (input.size() % 2) {  // tamanho do vetor é ímpar
      // int ultimo_index = input_size - 1;
      input.pop_back();
    }

    // tamanho do vetor é par
    int metade = input.size() / 2;
    Pint soma1, soma2;

    for (int i=0; i<metade; i++) {
      soma1 = soma1 + input[i];
      soma2 = soma2 + input[i+metade];
    }
    int comparacaoSomas = soma1.compare(soma2);
    if (comparacaoSomas == 0) {
      // return ultimo_index;
      return input.size();
    }
    else if (comparacaoSomas == -1) {
      // novo input com os elemntos da primeira metade;
      input.erase(metade, input.size()-1);
      return solution(input);
    }
    else {
      // chama solution com novo inout sendo os elemntos da segunda metade
      // tem q somar um número nos índices
      input.erase(input.begin(), input.begin()+metade);
      return metade + solution(input);
    }
  }

  return -1;
}
