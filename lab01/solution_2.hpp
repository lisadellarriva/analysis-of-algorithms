// MC458 Projeto e Analise de Algoritmos I – 2016/02 - Laboratório 1
// Aluna: Elisa Dell'Arriva - RA: 135551

#include "pint.hpp"
#include <vector>
#include <iostream>
using namespace std;

//retornar o indice (começando em 0) da embalagem mais leve
int solution(const vector<Pint> & input) {

  int index_inicio = 0;
  int index_fim = input.size()-1;
  int current_size = input.size();

  while (1) { // necessário pela tradução da recursão em iterativo
    if (current_size == 1) {
      return index_inicio;
    }
    else if (current_size == 2) {  // sobraram apenas dois a comparar
      if (input[index_inicio].compare(input[index_fim]) == 1) {
        return index_fim;
      }
      else {
        return index_inicio;
      }
    }
    else {
      if (current_size % 2) {  // tamanho atual do vetor é ímpar
        index_fim--;
        current_size--;
      }

      // tamanho atual do vetor é par
      int metade = current_size / 2;
      int index_metade = ((index_inicio + index_fim) / 2) + 1;
      Pint soma1, soma2;

      for (int i=0; i<metade; i++) {
        soma1 = soma1 + input[i+index_inicio];
        soma2 = soma2 + input[i+index_metade];
      }
      int comparacaoSomas = soma1.compare(soma2);
      if (comparacaoSomas == 0) { // então o mais leve é o elemento removido
        return (index_fim + 1);
      }
      else if (comparacaoSomas == -1) {
        // novo input com os elemntos da primeira metade;
        index_fim = index_metade - 1;
        current_size = metade;
      }
      else {
        // novo input com os elemntos da segunda metade
        index_inicio = index_metade;
        current_size = metade;
      }
    }
  }
  return -1;
}
