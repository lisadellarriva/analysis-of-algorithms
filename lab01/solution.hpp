// MC458 Projeto e Analise de Algoritmos I – 2016/02 - Laboratório 1
// Aluna: Elisa Dell'Arriva - RA: 135551
// ---------------------------------------------------------------------------
// Observação:
// não tive certeza se poderia criar uma função auxiliar pra fazer a recursão
// com mais parâmetros, então traduzi a recursão para um laço iterativo
// ---------------------------------------------------------------------------

#include "pint.hpp"
#include <vector>
#include <iostream>
using namespace std;

//retornar o indice (começando em 0) da embalagem mais leve
int solution(const vector<Pint> & input) {

  int index_inicio = 0;
  int index_fim = input.size()-1;
  int current_size = input.size();

  while (1) {
    // Casos bases
    if (current_size == 1) {  // apenas um elemento, então ele prórpio é o mais leve
      return index_inicio;
    }
    if (current_size == 2) {
      int comparacaoElementos = input[index_inicio].compare(input[index_inicio+1]);
      if (comparacaoElementos == -1) { // o mais leve é o primeiro elemento
        return index_inicio;
      }
      else if (comparacaoElementos == 1) {  // o mais leve é o segundo elemento
        return (index_inicio + 1);
      }
    }

    // Passo indutivo: dividir em três partes, sendo pelo menos duas de mesmo tamanho
    int terco = current_size / 3;
    int index_parte2_inicio = index_inicio + terco;
    int index_parte2_fim = index_parte2_inicio + terco - 1;
    Pint soma1, soma2;

    for (int i=0; i<terco; i++) {
      soma1 = soma1 + input[i+index_inicio];
      soma2 = soma2 + input[i+index_parte2_inicio];
    }
    int comparacaoSomas = soma1.compare(soma2);
    if (comparacaoSomas == 0) { // então o mais leve está na terceira parte
      current_size = index_fim - index_parte2_fim;
      index_inicio = index_parte2_fim + 1;
    }
    else if (comparacaoSomas == -1) { // então o mais leve está na primeira metade;
      index_fim = index_parte2_inicio - 1;
      current_size = terco;
    }
    else {  // então o mais leve está na segunda metade
      index_inicio = index_parte2_inicio;
      index_fim = index_parte2_fim;
      current_size = terco;
    }
  } // fim while
  return -1;
}
