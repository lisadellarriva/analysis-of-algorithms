//-------------------------------------------//
// MC458 - Análise de Algoritmo I - 2s2016
// Laboratório 4
// Aluna: Elisa Dell'Arriva - RA: 135551
//-------------------------------------------//

#include <vector>
#include <iostream>
#include <utility>
#include <stdlib.h>

using namespace std;

vector<int> torreMaximo(vector<int> &c, vector<int> &w, int W, int n) {
  vector< vector<int> > z(n+1, vector<int>(W+1));
  vector<int> resp(n);

  for (int d=0; d<W; d+=1) {
    z[0][d] = 0;
  }
  for (int k=1; k<n; k+=1) {
    z[k][0] = 0;
  }

  for (int k=1; k<=n; k+=1) {
    for (int d = 1; d<=W; d+=1) {
      z[k][d] = z[k-1][d];
      if ((w[k-1] <= d) && (c[k-1] + z[k-1][d-w[k-1]] > z[k][d])) {
        z[k][d] = c[k-1] + z[k-1][d-w[k-1]];
      }
    }
  }

  //recupera lista de empresas
  int k = n, d = W;
  while (k != 0) {
    if (z[k][d] == z[k-1][d]) {
      resp[k-1] = 0;
      k = k - 1;
    }
    else {
      resp[k-1] = 1;
      d = d - w[k-1];
      k = k - 1;
    }
  }

  return resp;
}

//retorna as empresas ( em qualquer ordem) de uma das torres. As empresas restantes são colocadas na outra torre.
vector<int> solution(vector<int> & input) {
  vector<int> items(input.size());
  vector<int> resp;

  int N = input.size();   // numero de empresas pedindo andaraes nas torres
  int maior = 0;          // maior quantidade de andares  pedidos por um empresa
  int indiceMaior = 0;    // indice da empresa que pediu maior quantidade de andares
  int media = 0;          // media aritmetica de andares por torre
  int itemsSize = 0;

  // determina o maior número de andar e a média de andares por torre
  for (int i=0; i<N; i+=1) {
    if (input[i] > maior) {
      maior = input[i];
      indiceMaior = i;
    }
    media = media + input[i];
  }
  if (media % 2) {
    media = (media / 2) + 1;
  }
  else {
    media = (media / 2);
  }

  if (maior > media) {  // uma das torres terá a empresa de maior número de andares
    items[0] = indiceMaior;
    items.resize(1);
  }
  else {  // faz problema da mochila com capacidade c = media
    resp = torreMaximo(input, input, media, N);

    int j = 0;
    for (int i=0; i<N; i+=1) {
      if (resp[i] == 1) {
        items[j] = i;
        itemsSize = itemsSize + 1;
        j = j + 1;
      }
    }
    items.resize(itemsSize);
  }

  return items;
}
